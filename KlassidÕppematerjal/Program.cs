﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidÕppematerjal
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Nimekiri.txt";
            string[] linesFromFile = System.IO.File.ReadAllLines(filename);

            List<Person> listOfPeople = new List<Person>();

            // siin me loeme failist listi
            foreach (string lineFromFile in linesFromFile)
            {

                listOfPeople.Add(new Person
                {
                    Nimi = lineFromFile.Split(',')[0],
                    IK = lineFromFile.Split(',')[1]
                });
            }
            // siin on meil list valmis


            // nüüd proovime selle valmis loetud listi välja trükkida
            foreach (Person person in listOfPeople)
            {
                Console.WriteLine(person);
                Console.WriteLine($"{person.Gender()} {person.Nimi}");
            }
            // siin on see välja trükitud

            double sum = 0;
            foreach (var x in listOfPeople) sum += x.Age();
            Console.WriteLine($"keskmine vanus on {sum / listOfPeople.Count}");


        }
    }
}

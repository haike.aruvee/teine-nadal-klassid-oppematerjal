﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidÕppematerjal
{
    enum Gender { Female, Male }
    class Person
    {
        public string Nimi;
        public string IK;

        public DateTime BirthDate()  // annab inimese sünnikuupäeva
        {
            // IK.Substring(0,1) 1 v 2 1800, 3-4 1900, 5-6 2000
            // IK.Substring(1,2) + aasta
            // IK.Substring(3,2) kuu
            // IK.Substring(5,2) päev

            string sajand = "";
            switch (this.IK.Substring(0, 1))
            {
                case "1": case "2": sajand = "19"; break;
                case "3": case "4": sajand = "20"; break;
                case "5": case "6": sajand = "21"; break;
                case "7": case "8": sajand = "22"; break;
            }

            string kp = sajand
                + IK.Substring(1, 2) + "/"
                + IK.Substring(3, 2) + "/"
                + IK.Substring(5, 2);
            return DateTime.Parse(kp);

        }

        public int Age() // annab inimese vanuse
        {
            return (DateTime.Today - this.BirthDate()).Days * 4 / 1461;
        }

        public Gender Gender()
        {
            // kuidas leida inimese sugu?
            return (Gender)(IK[0] % 2);
        }


        // kuidas inimene stringiks teha
        public override string ToString() => $"Inimene nimega {Nimi} (ik: {IK})";
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidÕppematerjal
{
    class Soiduk
    {
        public string Mark;
        public string Mudel;

        public int Rattaid;
        public int Vedavaid;
        public double Voimsus;

        public double RattaVoimsus()
        {
            return Voimsus / Vedavaid;

        }
    }
}
